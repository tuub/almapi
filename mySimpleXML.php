<?php

class MySimpleXMLElement extends SimpleXMLElement {

  public function addChildAttr($name, $value='', $attrs=[], $namespace = null){
    if (is_array($value)){ echo $name; print_r($value); }
    $child = $this->addChild($name, strtr($value,array("&" => "&amp;")), $namespace);
    foreach ($attrs as $att => $val)
      $child->addAttribute($att,$val);
    return $child;
  }

  public function getParent(){
    return current($this->xpath('parent::*'));
  }

  public function printXML(){
    $xmllint = popen('xmllint --format -', 'w');
    fwrite($xmllint,$this->asXML());
    pclose($xmllint);
  }
}

if (!function_exists('str_starts_with')) {
    function str_starts_with($haystack, $needle) {
        return (string)$needle !== '' && strncmp($haystack, $needle, strlen($needle)) === 0;
    }
}
if (!function_exists('str_ends_with')) {
    function str_ends_with($haystack, $needle) {
        return $needle !== '' && substr($haystack, -strlen($needle)) === (string)$needle;
    }
}
if (!function_exists('str_contains')) {
    function str_contains($haystack, $needle) {
        return $needle !== '' && mb_strpos($haystack, $needle) !== false;
    }
}
