<?php

require_once('mySimpleXML.php');

function getLink($url){

    $getopts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
        )
    );

    $getcont = stream_context_create($getopts);

    if (substr($url,0,strlen(ALMAHOST)) == ALMAHOST)
        return simplexml_load_string(file_get_contents($url, FALSE, $getcont), 'MySimpleXMLElement');
    else
        return false;
}

function putLink($url, $xml){
    $putopts = array('http' =>
        array(
            'method'  => 'PUT',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY)
        )
    );
    if (is_string($xml))
      $putopts['http']['content'] = $xml;
    else
      $putopts['http']['content'] = $xml->asXML();

    $putcont = stream_context_create($putopts);
    if (substr($url,0,strlen(ALMAHOST)) == ALMAHOST)
        return simplexml_load_string(file_get_contents($url, FALSE, $putcont), 'MySimpleXMLElement');
    else
        return false;
}

function getTitle($mmsID){

    $getopts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
        )
    );

    $getcont = stream_context_create($getopts);
    if (substr($mmsID,0,2) == 'AC'){
        $url = ALMAHOST.BIBS.'?other_system_id='.$mmsID;
        return simplexml_load_string(file_get_contents($url, FALSE, $getcont), 'MySimpleXMLElement')->bib;
    } else {
        $url = ALMAHOST.BIBS.$mmsID;
        return simplexml_load_string(file_get_contents($url, FALSE, $getcont), 'MySimpleXMLElement');
    }
}

function putTitle($mmsID,$xml){
    $putopts = array('http' =>
        array(
            'method'  => 'PUT',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
            'content' => $xml->asXML()
        )
    );

    $putcont = stream_context_create($putopts);
    $ret_xml = file_get_contents(ALMAHOST.BIBS.$mmsID, FALSE, $putcont);
    if ($ret_xml)
      return simplexml_load_string($ret_xml, 'MySimpleXMLElement');
    else
      return false;
}

function newTitle($xml){
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
            'content' => $xml->asXML()
        )
    );

    $context = stream_context_create($opts);

    $ret_xml = file_get_contents(ALMAHOST.BIBS,FALSE,$context);
    if ($ret_xml)
      return simplexml_load_string($ret_xml, 'MySimpleXMLElement');
    else
      return false;
}

function getHoldings($mms_id,$holding=false){
    $getopts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
        )
    );
    $getcont = stream_context_create($getopts);

    $url = ALMAHOST.BIBS.$mms_id.'/holdings';
    if ($holding) $url .= '/'.$holding;
    return simplexml_load_string(file_get_contents($url, FALSE, $getcont), 'MySimpleXMLElement');
}

function putHoldings($mms_id,$holding,$xml){
    $putopts = array('http' =>
        array(
            'method'  => 'PUT',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
            'content' => $xml->asXML()
        )
    );
    $putcont = stream_context_create($putopts);

    $url = ALMAHOST.BIBS.$mms_id.'/holdings'.'/'.$holding;
    return simplexml_load_string(file_get_contents($url, FALSE, $putcont), 'MySimpleXMLElement');
}

function getItem($mmsId, $holId, $itemId){
    return getLink(ALMAHOST.sprintf(ITEMS,$mmsId, $holId, $itemId));
}

function putItem($mmsId, $holId, $itemId, $xml){
    return putLink(ALMAHOST.sprintf(ITEMS,$mmsId, $holId, $itemId),$xml);
}

function addToSet($set,$ids){
    global $mail;
    if (!is_numeric($set)){
        $opts = array('http' =>
                array(
                    'method'  => 'GET',
                    'header'  => array ("Content-Type: application/xml",
                                        "Authorization: apikey ".APIKEY)
                )
        );

        $context = stream_context_create($opts);

        $url = ALMAHOST.SETS.'?q=name~'.urlencode($set);
        $xml = simplexml_load_string(file_get_contents($url,FALSE,$context));
        $set = (string)$xml->set->id;
    }
    if (!is_numeric($set)) return false;

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY)
        )
    );

    $context = stream_context_create($opts);
    $url = ALMAHOST.SETS.'/'.$set.'?op=add_members';
    foreach (array_chunk($ids,100) as $cent_ids){
        $postdata = "<set>\n  <members>\n    <member><id>".implode("</id></member>\n    <member><id>",$cent_ids)."</id></member>\n  </members>\n</set>";
        stream_context_set_option ( $context , 'http' , 'content', $postdata );
        $xml = simplexml_load_string(file_get_contents($url,FALSE,$context));
        if (!$xml) $mail .= "API Fehler beim Erweitern des Sets (".implode(' ',$cent_ids).")\n";
    }
}

// get or iterate set members
// function argument $member is simpleXml and depends on set type
function setMembers($setid,$function=null){
    $opts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array ("Authorization: apikey ".APIKEY)
        )
    );

    $set_members = array();
    $context = stream_context_create($opts);
    $url = ALMAHOST.SETS.'/'.$setid.'/members?limit=10';
    // rückwärts weil $function das set eventuell reduziert
    $xml = simplexml_load_string(file_get_contents(ALMAHOST.SETS.'/'.$setid.'/members?limit=1',FALSE,$context));
    $tot_recs = intval($xml['total_record_count']) -1;
    $offset = $tot_recs - $tot_recs % 100;
    do {
        $xml = simplexml_load_string(file_get_contents($url.'&offset='.$offset,FALSE,$context), 'MySimpleXMLElement');
        foreach ($xml->member as $member)
            if (is_callable($function))
                $function($member);
            else
                $set_members[] = (string)$member->id;
        $offset -= 10;
    } while ($offset >= 0);
    return $set_members;
}

// get or iterate all e-collections
// function argument $member is simpleXml and depends on set type
function getECollections($function=null){
    $opts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array ("Authorization: apikey ".APIKEY)
        )
    );

    $eColls = array();
    $context = stream_context_create($opts);
    $url = ALMAHOST.ECOLL.'?limit=100';
    // rückwärts weil $function das set eventuell reduziert
    $xml = simplexml_load_string(file_get_contents(ALMAHOST.ECOLL.'?limit=1',FALSE,$context));
    $tot_recs = intval($xml['total_record_count']) -1;
    $offset = $tot_recs - $tot_recs % 100;
    do {
        $xml = simplexml_load_string(file_get_contents($url.'&offset='.$offset,FALSE,$context));
        foreach ($xml->electronic_collection as $eColl)
            if (is_callable($function))
                $function($eColl);
            else
                $eColls[] = (string)$eColl->id;
        $offset -= 100;
    } while ($offset >= 0);
    return $eColls;
}

function getCollection($coll_id){
    $getopts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
        )
    );
    $getcont = stream_context_create($getopts);

    $url = ALMAHOST.ECOLL.$coll_id;
    return simplexml_load_string(file_get_contents($url, FALSE, $getcont), 'MySimpleXMLElement');
}

function putCollection($coll_id,$col){
    $putopts = array('http' =>
        array(
            'method'  => 'PUT',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
            'content' => $col->asXML()
        )
    );
    $putcont = stream_context_create($putopts);

    $url = ALMAHOST.ECOLL.$coll_id;
    return simplexml_load_string(file_get_contents($url, FALSE, $putcont), 'MySimpleXMLElement');
}

function getServices($coll_id){
    $getopts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
        )
    );
    $getcont = stream_context_create($getopts);

    $url = ALMAHOST.ECOLL.$coll_id.'/e-services';
    return simplexml_load_string(file_get_contents($url, FALSE, $getcont), 'MySimpleXMLElement');
}

function getPortfolios($mms_id,$portfolioID=false,$function=false){
    $getopts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
        )
    );
    $getcont = stream_context_create($getopts);

    if (substr($mms_id,0,4) == 'http'){
        $url = $mms_id.'?limit=100';
        $getopts = array('http' =>
            array(
                'method'  => 'GET',
                'header'  => array ("Content-Type: application/xml",
                                    "Authorization: apikey ".APIKEY),
            )
        );
        $getcont = stream_context_create($getopts);
        $tot_recs = $portfolioID;
        $offset = $tot_recs - $tot_recs % 100;
        $portfolios = [];
        do {
            $xml = simplexml_load_string(file_get_contents($url.'&offset='.$offset,FALSE,$getcont));
            foreach ($xml->portfolio as $portfolio)
                if (is_callable($function))
                    $function($portfolio);
                else
                    $portfolios[] = (string)$portfolio->id;
            $offset -= 100;
        } while ($offset >= 0);
        return $portfolios;
    } else {
        $url = ALMAHOST.BIBS.$mms_id.'/portfolios';
        if ($portfolioID) $url .= '/'.$portfolioID;
        return simplexml_load_string(file_get_contents($url, FALSE, $getcont), 'MySimpleXMLElement');
    }
}

    

function newPortfolio($mms_id,$link,$coll_id = false,$service_id = false){
    $portfolio = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<portfolio>
  <is_local>true</is_local>
  <is_standalone>true</is_standalone>
  <resource_metadata>
    <mms_id link="'.ALMAHOST.BIBS.$mms_id.'">'.$mms_id.'</mms_id>
  </resource_metadata>
  <electronic_collection>
    <id/>
    <service/>
  </electronic_collection>
  <availability desc="Available">11</availability>
  <material_type desc="Book">BOOK</material_type>
  <activation_date>'.date("Y-m-d").'Z</activation_date>
  <expected_activation_date/>
  <library link="'.ALMAHOST.EBOOK.'">EBOOK</library>
  <access_type/>
  <linking_details>
    <url/>
    <url_type>static</url_type>
    <dynamic_url/>
    <static_url>jkey='.$link.'</static_url>
    <parser_parameters/>
    <parser_parameters_override/>
    <proxy_enabled desc="No">false</proxy_enabled>
    <proxy/>
  </linking_details>
  <coverage_details>
    <coverage_in_use desc="Only local">0</coverage_in_use>
    <global_date_information/>
    <local_date_information/>
  </coverage_details>
  <po_line/>
  <license/>
  <interface>
    <name/>
  </interface>
  <pda link=""/>
  <authentication_note/>
  <public_note/>
</portfolio>';

    if ($coll_id !== false && $service_id !== false){
      $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
            'content' => $portfolio
        )
      );
      $url = ALMAHOST.ECOLL.$coll_id.'/e-services/'.$service_id.'/portfolios';
    } else {

        $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
            'content' => $portfolio
        )
      );
      $url = ALMAHOST.BIBS.$mms_id.'/portfolios';
    }
    $context = stream_context_create($opts);
    $stream = fopen($url, 'r', false, $context);
    $xml = simplexml_load_string(stream_get_contents($stream));
    if ($xml === false){ echo "<pre>"; var_dump(stream_get_meta_data($stream)); echo $url,"\n",$portfolio,"</pre>\n"; }
    fclose($stream);
    return $xml;
}

function getLibraries($lang=false, $id = false){
    $getopts = array('http' =>
        array(
            'method'  => 'GET',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
        )
    );
    $getcont = stream_context_create($getopts);
    $url = ALMAHOST.LIBRARIES.($id?$id:'').($lang?"?lang=$lang":'');
    return simplexml_load_string(file_get_contents($url, FALSE, $getcont), 'MySimpleXMLElement');
}

function getLocations($libid, $lang=false, $id = false){
    $getopts = array('http' =>
         array(
            'method'  => 'GET',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
        )
    );
    $getcont = stream_context_create($getopts);
    $url = ALMAHOST.sprintf(LOCATIONS,$libid).($id?$id:'').($lang?"?lang=$lang":'');
    return simplexml_load_string(file_get_contents($url, FALSE, $getcont), 'MySimpleXMLElement');
}

function putLocation($libid, $lang, $id, $location, $verbose=false){
    $putopts = array('http' =>
        array(
            'method'  => 'PUT',
            'header'  => array ("Content-Type: application/xml",
                                "Authorization: apikey ".APIKEY),
            'content' => $location->asXML()
        )
    );
    $putcont = stream_context_create($putopts);
    $url = ALMAHOST.sprintf(LOCATIONS,$libid).$id.($lang?"?lang=$lang":'');
    if ($verbose) echo "curl -X PUT -H '".implode("' -H '",$putopts['http']['header'])."' '$url' --data '".strtr($location->asXML(),["\n" => ''])."'\n";
    return simplexml_load_string(file_get_contents($url, FALSE, $putcont), 'MySimpleXMLElement');
}

function check_NZ($doi = '', $isbn = ''){
    $query = [];
    if (!empty($doi))
        $query[] = "alma.digital_object_identifier=$doi";
    if (!empty($isbn)){
        if (is_object($isbn))
          $query[] = "alma.isbn".$isbn->value;
        else
          $query[] = "alma.isbn=$isbn";
    }
    if (!empty($query)){
        $nzdss = simplexml_load_file('https://obv-at-ubtuw.alma.exlibrisgroup.com/view/sru/43ACC_NETWORK?version=1.2&operation=searchRetrieve&'.http_build_query(['query' => implode(' or ',$query)]), 'MySimpleXMLElement');
        $nzdss->registerXPathNamespace('slim','http://www.loc.gov/MARC21/slim');
        $f007 = $nzdss->xpath('//slim:controlfield[@tag="007"]');
        $f020 = $nzdss->xpath('//slim:datafield[@tag="020"]/slim:subfield[@code="a"]');
        $f776 = $nzdss->xpath('//slim:datafield[@tag="776"]/slim:subfield[@code="z" or @code="o"]');
        $acs = $nzdss->xpath('//slim:controlfield[@tag="009"]');
        return(array_map(function($x){return (string)$x;}, $acs));
    }
}
